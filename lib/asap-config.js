var _ = require('lodash');
var fs = require('mz/fs');
var q = require('q');

function readFileIfExists(path) {
    return q()
        .then(function() {
            if (path) {
                return fs.exists(path);
            }
        })
        .then(function (exists) {
            if (exists) {
                return fs.readFile(path, 'utf8');
            }
        });
}

function enrichOptions(options) {
    return q()
        .then(function() {
            if (options.expiry) {
                options.expiry = parseInt(options.expiry);
            }
        })
        .then(function() {
            return readFileIfExists(options.privateKeyFile);
        })
        .then(function (fileContent) {
            if(fileContent) {
                options.privateKey = fileContent;
            }
            return options;
        });
}

function readConfigFile(options) {
    return q()
        .then(function () {
            return readFileIfExists(options.configFile);
        })
        .then(function (data) {
            return data || readFileIfExists('./.asap-config');
        })
        .then(function (data) {
            return data || readFileIfExists('./asap-config');
        });
}

module.exports = {

    getFromOptions: function getFromOptions(options) {

        return q.all([readConfigFile(options), enrichOptions(options)])
            .spread(function (asapFileContent, options) {
                if (asapFileContent) {
                    // eslint-disable-next-line no-control-regex
                    var config = JSON.parse(asapFileContent.toString().replace(/[\x00-\x1F\x7F-\x9F]/g, ''));
                    var baseConfigKeys = ['issuer','sub','audience','kid','privateKey','expiry'];
                    for(i in baseConfigKeys) {
                        k = baseConfigKeys[i];
                        if(options[k]) {
                            config[k] = options[k];
                        }
                    }
                    return config;
                }
                return options;
            });
    },

    validate: function validate(config) {
        var validateString = function (value) { return _.isString(value) && value; };
        var validateArray = function (value) { return _.isArray(value) && _.filter(value).length > 0; };

        var validators = [
            _.partial(function (value) { return validateArray(value) || validateString(value); }, config.audience),
            _.partial(function (value) { return !value || validateString(value); }, config.sub),
            _.partial(validateString, config.issuer),
            _.partial(validateString, config.kid),
            _.partial(validateString, config.privateKey)
        ];

        var allRequiredConfigProvided = _.every(validators, function (validator) { return validator(); });
        if (!allRequiredConfigProvided) {
            return q.reject(
                'Issuer, audience, kid and privateKey must be provided in the config file or passed in as options'
            );
        }
        return q(config);
    },
    
    save: function save(configFile, config) {
        return fs.writeFile(configFile, JSON.stringify(config, null, 2));
    }
};