var readLineSync = require('readline-sync');
var fs = require('fs');
var spawn = require('child_process').spawn;
var _ = require('lodash');
var asapConfig = require('./asap-config');
var jwtUtil = require('./jwt-utils');
var q = require('q');
var NodeRSA = require('node-rsa');
var format = require('string-template');
var Proxy = require('http-mitm-proxy');
var expandTilde = require('expand-tilde');

function executeCommand(command) {
    var deferred = q.defer();

    var child = spawn(command[0], command.slice(1), { stdio: 'inherit' });
    child.on('close', function (code) {
        if (code === 0) {
            deferred.resolve();
        } else {
            deferred.reject('Child process exited with code: ' + code);
        }
    });
    return deferred.promise;
}

function parseAudience(options) {
    var audience = options.audience
        || readLineSync.question('Specify the JWT Audience (comma separated for more than one): ');
    return _.split(audience, ',').map(_.trim);
}

function parseKid(options) {
    var kid = options.kid || readLineSync.question('Specify the JWT Key ID: ');
    return _.trim(kid);
}

function parseIssuer(options) {
    var issuer = options.issuer || readLineSync.question('Specify the JWT Issuer: ');
    return _.trim(issuer);
}

function parseSubject(options) {
    var subject = options.sub || readLineSync.question('Specify the JWT Subject (leave blank to use issuer): ');
    return _.trim(subject);
}

function parseConfigFile(options) {
    var configFile = options.configFile || readLineSync.question(
        'Specify the ASAP Config File (defaults to .asap-config): ',
        {defaultInput: './.asap-config'}
    );
    return _.trim(configFile);
}

function parseExpiry(options) {
    return parseInt(options.expiry) || readLineSync.questionInt(
        'Specify the JWT token expiry in seconds (defaults to 60 sec): ',
        {defaultInput: 60}
    );
}

function parsePrivateKey(options) {
    var privateKey;
    if (options.privateKeyFile || options.privateKey) {
        privateKey = options.privateKey || fs.readFileSync(options.privateKeyFile, 'utf8');
    } else {
        var privateKeyFile = readLineSync.question(
            'Specify the Private Key File (If you want to specify the private key directly, leave this blank): '
        );

        if (!privateKeyFile) {
            privateKey = readLineSync.question(
                'Specify the Private Key (Please replace the new lines with literal \\n )'
            );
        } else {
            privateKey = fs.readFileSync(privateKeyFile, 'utf8');
        }
    }
    return _.trim(privateKey);
}

module.exports = {
    init: function (options) {
        return function () {

            var config = {
                issuer: parseIssuer(options),
                sub: parseSubject(options),
                kid: parseKid(options),
                audience: parseAudience(options),
                privateKey: parsePrivateKey(options),
                expiry: parseExpiry(options)
            };
            if (!config.sub) {
                config.sub = config.issuer;
            }
            var configFile = parseConfigFile(options);

            return q()
                .then(function () {
                    return asapConfig.validate(config);
                })
                .then(function () {
                    return asapConfig.save(configFile, config);
                })
                .then(function () {
                    console.log(
                        'ASAP Auth config file initialised successfully... curl away!!! (Well.. "asap curl" really)'
                    );
                });
        };
    },
    
    token: function (programOptions) {
        return function () {
            return q()
                .then(function () {
                    return asapConfig.getFromOptions(programOptions);
                })
                .then(asapConfig.validate)
                .then(jwtUtil.getJwtAuthHeader)
                .then(function (authHeader) {
                    console.log(authHeader);
                });
        };
    },
    
    curl: function (programOptions, processArgs) {
        return function () {
            return q()
                .then(function () {
                    return asapConfig.getFromOptions(programOptions);
                })
                .then(asapConfig.validate)
                .then(jwtUtil.getJwtAuthHeader)
                .then(function (authHeader) {
                    var curlArgs = _.takeRightWhile(processArgs, function (item) {
                        return item !== 'curl';
                    });
                    return ['curl', '-H', 'Authorization: ' + authHeader].concat(curlArgs);
                })
                .then(executeCommand);
        };
    },
    
    http: function (programOptions, processArgs) {
        return function () {
            return q()
                .then(function () {
                    return asapConfig.getFromOptions(programOptions);
                })
                .then(asapConfig.validate)
                .then(jwtUtil.getJwtAuthHeader)
                .then(function (authHeader) {
                    var httpArgs = _.takeRightWhile(processArgs, function (item) {
                        return item !== 'http';
                    });
                    return ['http'].concat(httpArgs).concat(['Authorization:' + authHeader]);
                })
                .then(executeCommand);
        };
    },

    validate: function (programOptions) {
        return function (resourceServerAudience, publicKeyBaseUrl) {
            return q()
                .then(function () {
                    return asapConfig.getFromOptions(programOptions);
                })
                .then(asapConfig.validate)
                .then(function (options) {
                    return q()
                        .then(function () {
                            return jwtUtil.getJwtToken(options);
                        })
                        .then(function (token) {
                            var validateOptions = _.extend({
                                resourceServerAudience: resourceServerAudience,
                                publicKeyBaseUrl: publicKeyBaseUrl
                            }, options);

                            return jwtUtil.validate(token, validateOptions);
                        })
                        .then(console.log);
                });
        };
    },

    exportAsDataUri: function(programOptions) {
        return function () {
            return q(asapConfig.getFromOptions(programOptions))
                .then(asapConfig.validate)
                .then(function (options) {
                    var key = new NodeRSA();
                    key.importKey(options.privateKey, 'pkcs1');

                    return format('data:application/pkcs8;kid={kid};base64,{pkcs8}', {
                        kid: encodeURIComponent(options.kid),
                        pkcs8: key.exportKey('pkcs8-private-der').toString('base64')
                    });
                })
                .then(function (dataUri) {
                    console.log(dataUri);
                });
        };
    },

    proxy: function (programOptions) {
        return function (commandOptions) {
            return q(asapConfig.getFromOptions(programOptions))
                .then(asapConfig.validate)
                .then(function (asapOptions) {
                    var proxy = Proxy();

                    proxy.onError(function (ctx, err) {
                        console.error(err);
                    });

                    proxy.onRequest(function (ctx, callback) {
                        jwtUtil.getJwtAuthHeader(asapOptions)
                            .then(function (asapHeader) {
                                ctx.proxyToServerRequestOptions.headers['authorization'] = asapHeader;
                                console.info(format('Forwarded {method} {host}{path}', {
                                    method: ctx.proxyToServerRequestOptions.method,
                                    host: ctx.proxyToServerRequestOptions.host,
                                    path: ctx.proxyToServerRequestOptions.path
                                }));
                            }).nodeify(callback);
                    });

                    var port =  commandOptions.port || 8888;

                    proxy.listen({
                        port: {
                            port: port,
                            host: 'localhost'
                        },
                        sslCaDir: expandTilde('~/.http-mitm-proxy'),
                        silent: true
                    }, function (err) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.info(format('Listening on port {port}, will generate JWT tokens for {kid}', {
                                'port': port,
                                kid: asapOptions.kid
                            }));
                        }
                    });
                });
        };
    }
};
