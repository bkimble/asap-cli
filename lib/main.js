require('any-promise/register/q');

var program = require('commander');
var commands = require('./commands');
var q = require('q');

function logErrorsAndResolve(fn) {
    return function () {
        return q(arguments)
            .then(function (args) {
                return fn.apply(null, args);
            })
            .catch(console.log)
            .done();
    };
}

module.exports = function main() {
    program
        .usage('[options] <command>')
        .option('--config-file [configFile]', 'ASAP Config File, defaults to .asap-config')
        .option('--private-key [privateKey]', 'Private Key')
        .option('--private-key-file [privateKeyFile]', 'Private Key File')
        .option('--issuer [issuer]', 'JWT issuer')
        .option('--sub [sub]', 'JWT subject')
        .option('--audience [audience]', 'JWT Audience, comma separated for more than one')
        .option('--kid [kid]', 'JWT Key ID')
        .option('--expiry [expiry]', 'Expiry in seconds');

    program
        .command('init')
        .description('Pre-configure an ASAP config file which can be used to generate asap tokens')
        .action(logErrorsAndResolve(commands.init(program)));

    program
        .command('token')
        .description('Generate the ASAP Authorization header')
        .action(logErrorsAndResolve(commands.token(program)));

    program
        .command('curl')
        .description('Execute curl commands with auto injected ASAP Auth Header')
        .allowUnknownOption()
        .action(logErrorsAndResolve(commands.curl(program, process.argv)));

    program
        .command('http')
        .description(
            'Execute HTTPie commands with auto injected ASAP Auth Header. ' +
            'Prerequisite: HTTPie needs to be installed (https://github.com/jkbrzt/httpie)'
        )
        .allowUnknownOption()
        .action(logErrorsAndResolve(commands.http(program, process.argv)));

    program
        .command('validate <resourceServerAudience> <publicKeyBaseUrl>')
        .description(
            'Validate the generated ASAP token against a public key server and an expected ' +
            'server audience. This comes handy to check if the asap client configs are setup properly'
        )
        .action(logErrorsAndResolve(commands.validate(program)));


    program
        .command('export-as-data-uri')
        .description('Export the private key as a data URI')
        .action(logErrorsAndResolve(commands.exportAsDataUri(program)));

    program
      .command('proxy')
      .description(
        'Run an HTTP proxy that adds the ASAP Auth Header, will only bind to localhost. ' +
        'Note that this will create a ~/.http-mitm-proxy directory containing the generated certificates.'
      )
      .option('--port [port]', 'The port of the proxy.')
      .action(logErrorsAndResolve(commands.proxy(program)));

    program.parse(process.argv);

    if (!process.argv.slice(2).length) {
        program.help();
    }
};
