var jwtAuthentication = require('jwt-authentication');
var q = require('q');

var generator = jwtAuthentication.client.create();

var generateAuthorizationHeader = q.nfbind(generator.generateAuthorizationHeader.bind(generator));
var generateToken = q.nfbind(generator.generateToken.bind(generator));

function parseTokenOptions(options) {
    var privateKey = options.privateKey.replace(/\\n/g, '\n').replace(/"/g, '');
    return {privateKey: privateKey, kid: options.kid, expiresInSeconds: options.expiry||60};
}

function parseClaims(options) {
    let claims = {iss: options.issuer, sub: options.sub || options.issuer, aud: options.audience};
    var baseConfigKeys = ['issuer','sub','audience','kid','privateKey','expiry'];    
    for(k in options) {
        if(baseConfigKeys.indexOf(k) === -1) {
            claims[k] = options[k];
        }
    }
    return claims;
}

module.exports = {
    /**
     * Returns a promise with the headerValue. Add this
     * to the Authorization header of the request object
     * @param options having the following properties
     *  audience,
     *  issuer,
     *  kid,
     *  privateKey,
     *  expiry
     */
    getJwtAuthHeader: function (options) {
        return generateAuthorizationHeader(parseClaims(options), parseTokenOptions(options));
    },

    getJwtToken: function (options) {
        return generateToken(parseClaims(options), parseTokenOptions(options));
    },
    
    validate: function (token, options) {
        var authenticator = jwtAuthentication.server.create({
            publicKeyBaseUrl: options.publicKeyBaseUrl,
            resourceServerAudience: options.resourceServerAudience,
            ignoreMaxLifeTime: true
        });
        var authorizedSubjects = [options.issuer];
        var validate = q.nfbind(authenticator.validate.bind(authenticator));

        return validate(token, authorizedSubjects)
            .then(function (claims) {
                return 'Token Validated Successfully. The claims are\n' + JSON.stringify(claims, null, '  ');
            })
            .catch(function (error) {
                return 'Token validation failed: ' + error.message;
            });
    }
};