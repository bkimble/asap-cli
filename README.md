# ASAP Cli

> A command line utility for generating ASAP (JWT tokens as per the Atlassian Service Authentication Protocol) token and making curl calls with the same.

The utility generates an ASAP token from a pre-configured config file (or passed in options). This token can be 
printed out or used to make a curl call directly.

For more information on ASAP and JWT see

* http://s2sauth.bitbucket.org/
* https://tools.ietf.org/html/rfc7519

## Installation

    npm install -g asap-cli

## Usage

```
 $ asap

  Usage: asap [options] <command>


  Commands:

    init                 Pre-configure an ASAP config file which can be used to generate asap tokens
    token                Generate the ASAP Authorization header
    curl                 Execute curl commands with auto injected ASAP Auth Header
    http                 Execute HTTPie commands with auto injected ASAP Auth Header. Prerequisite: HTTPie needs to be installed (https://github.com/jkbrzt/httpie)
    export-as-data-uri   Export the private key as a data URI
    proxy [options]      Run an HTTP proxy that adds the ASAP Auth Header, will only bind to localhost. Note that this will create a ~/.http-mitm-proxy directory containing the generated certificates.

  Options:

    -h, --help                           output usage information
    --config-file [configFile]           ASAP Config File, defaults to .asap-config
    --private-key [privateKey]           Private Key
    --private-key-file [privateKeyFile]  Private Key File
    --issuer [issuer]                    JWT issuer
    --sub [subject]                      JWT subject
    --audience [audience]                JWT Audience, comma separated for more than one
    --kid [kid]                          JWT Key ID
    --expiry [expiry]                    Expiry in seconds
```

## Commands

### init

Initialise the config file which can be reused by the other commands

**Examples**

```
$ asap init
Specify the JWT Issuer: token_issuer
Specify the JWT Subject (leave blank to use issuer): subject
Specify the JWT Key ID: key_id
Specify the JWT Audience (comma separated for more than one): intended_audience1,intended_audience2
Specify the Private Key File (If you want to specify the private key directly, leave this blank): pk.pem
Specify the JWT token expiry in seconds (defaults to 60 sec): 120
Specify the ASAP Config File (defaults to .asap-config): 
ASAP Auth config file initialised successfully... curl away!!! (Well.. "asap curl" really)
```

### token

Generate the ASAP token from the pre-configured config file (defaults to .asap-config). The config file or the values read from the config file can be overriden by specifying the global options. The output can be directly copied to the "Authorization" header. 

**Examples**

```
$ asap token
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImtleV9pZCJ9.eyJpc3MiOiJ0b2tlbl9pc3N1ZXIiLCJzdWIiOiJ0b2tlbl9pc3N1ZXIiLCJhdWQiOiJpbnRlbmRlZF9hdWRpZW5jZSIsImp0aSI6ImIxNDgwZDdkNWM5ZTVkMjMwOGUyNzM1MWM4OWI3YjBjNDMzZThkNmYiLCJpYXQiOjE0NjQ5MjQxODcsImV4cCI6MTQ2NDkyNDE4N30.nXdaYTmrHr99miufkHZxKA4kJC6hh2I0x7eO4ELQItJf_67RsnLA13ECT0iDo86tNqniPLwmnEMS9RCRzbumEdiR6vpGGaXM3LV0Y95BRK9YWiqQCWZs7SUstFnTTqI7UjyOtN22kwLbeHLmDRd_YMo2kLBY4Ago0BCNlsCoUKLRiEeMjuO0A_rtKnGuNo7fxGi0XqZVaPC2arHBks-6DmP3mz3FpvT-BO4fDFTA57Bb9HhnTBO6foEyr5q9VTU--Ov_kl8v_Fpi5XlGjWaiPzEfYbyXm1GPQCRu3107lqMShcSTtlkcr73WFNkaK2GPWmyGspuuRFP9tHcxg
```

Override the expiry set in the config file
```
$ asap --expiry 200 token
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImtleV9pZCJ9.eyJpc3MiOiJ0b2tlbl9pc3N1ZXIiLCJzdWIiOiJ0b2tlbl9pc3N1ZXIiLCJhdWQiOiJpbnRlbmRlZF9hdWRpZW5jZSIsImp0aSI6ImIxNDgwZDdkNWM5ZTVkMjMwOGUyNzM1MWM4OWI3YjBjNDMzZThkNmYiLCJpYXQiOjE0NjQ5MjQxODcsImV4cCI6MTQ2NDkyNDE4N30.nXdaYTmrHr99miufkHZxKA4kJC6hh2I0x7eO4ELQItJf_67RsnLA13ECT0iDo86tNqniPLwmnEMS9RCRzbumEdiR6vpGGaXM3LV0Y95BRK9YWiqQCWZs7SUstFnTTqI7UjyOtN22kwLbeHLmDRd_YMo2kLBY4Ago0BCNlsCoUKLRiEeMjuO0A_rtKnGuNo7fxGi0XqZVaPC2arHBks-6DmP3mz3FpvT-BO4fDFTA57Bb9HhnTBO6foEyr5q9VTU--Ov_kl8v_Fpi5XlGjWaiPzEfYbyXm1GPQCRu3107lqMShcSTtlkcr73WFNkaK2GPWmyGspuuRFP9tHcxg
```

### additional claims

Any extra line in the ASAP configuration file gets added to the JWT as a claim.


### curl

Make curl calls with the ASAP token (generated from the config file) set as the Authorization header. All the 
arguments meant for the curl command are passed through to curl. So this will support all the options that curl 
supports. The config file or the values read from the config file can be overriden by specifying the global options. 

**Examples**

```
$ asap curl -X PUT -d "foo=bar" "https://authenticated-service.company.com/item" -v
> PUT /item HTTP/1.1
> User-Agent: curl/7.37.1
> Host: authenticated-service.company.com
> Accept: */*
> Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImtleV9pZCJ9.eyJpc3MiOiJ0b2tlbl9pc3N1ZXIiLCJzdWIiOiJ0b2tlbl9pc3N1ZXIiLCJhdWQiOiJpbnRlbmRlZF9hdWRpZW5jZSIsImp0aSI6ImNlOThjN2IyYzUwMTJlYzNhMzBkNjhlNWM0ZTMxZGU0MTgzOGU2YzEiLCJpYXQiOjE0NjQ5MjQ3MTksImV4cCI6MTQ2NDkyNDcxOX0.a45Tz3hSwBpjK91AI46VpZ9DlQ7mA0IisqT7OymSSZiY5Qa1NtW3ZKmgAUFEj4C-MFaC0gB5j2g5-Lu_LjHtCO5KxOvF_eig8nIs3MH57k_jDFJCPioL9nxGbiHYfQd9_InrDVOqz8H_bZeSvnqq94c2MwDhvibYPPMRMEeY1aG3oFWA5m8andeeP1F1799TFzbQZXd0Iv6BOjM35ujAeAGHTqj8wZOEczSoOcXY0dPnrXcPTu9ZLkskhtDYiVHyZHTvA_0ugXX4Wq6D7KZpUbefylS6Be8BrbCscIFyuVDNEyQi5MBIDTGQ8P5Xgn_5A00nGbuiLP63iX-zCp00NQ
> 
< HTTP/1.1 401 Unauthorized
< Content-Type: application/json

```

### http

Make HTTPie calls with the ASAP token (generated from the config file) set as the Authorization header. All the 
arguments meant for the HTTPie command are passed through. So this will support all the options that HTTPie supports. 
The config file or the values read from the config file can be overriden by specifying the global options.

Prerequisite: HTTPie needs to be installed. See https://github.com/jkbrzt/httpie for installation and usage.

**Examples**

```
$ asap http PUT https://authenticated-service.company.com/item X-API-Token:123 foo=bar
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 26
Content-Type: application/json; charset=utf-8

{
    "data": "Some data"
}

```

### proxy

Runs an HTTP(S) proxy (defaults to port 8888) which will add the ASAP token as Authorization header to forwarded requests. 
Note that this terminates TLS since the requests have to be modified, storing the generated certificates in `~/.http-mitm-proxy`.

```
$ asap proxy --port 8889
Listening on port 8889, will generate JWT tokens for user/1481174136
```

Now `localhost:8889` can be used as a proxy server which will add the Authorization headers to the forwarded requests.

Example:
```
$ curl --proxy 127.0.0.1:8888 --insecure https://www.google.com -v

* Rebuilt URL to: https://www.google.com/
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to 127.0.0.1 (127.0.0.1) port 8888 (#0)
* Establish HTTP proxy tunnel to www.google.com:443
> CONNECT www.google.com:443 HTTP/1.1
> Host: www.google.com:443
> User-Agent: curl/7.37.1
> Proxy-Connection: Keep-Alive
>
< HTTP/1.1 200 OK
<
* Proxy replied OK to CONNECT request
* TLS 1.2 connection using TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256
* Server certificate: www.google.com
> GET / HTTP/1.1
> User-Agent: curl/7.37.1
> Host: www.google.com
> Accept: */*
>
< HTTP/1.1 302 Found
< cache-control: private
< content-type: text/html; charset=UTF-8
< location: https://www.google.com.au/?gfe_rd=cr&ei=Kih4WO3mMq3r8AeZoangAw

* Closing connection 0

```

## Development guide

### Install dependencies

```
npm install
```

### Useful commands

```
# Run all checks
npm run check

# Run just the jasmine tests
npm test

# Run just the linter
npm run lint
```


### Perform a release

```
npm version 99.98.97
npm publish
git push
git push --tags
```