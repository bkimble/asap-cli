var requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
var q = require('q');

describe('unit/asap-config', function () {

    var asapConfig;
    var mockFs;

    const asapConfigFileContent = '{' +
        '"issuer": "issuer",' +
        '"sub": "subject",' +
        '"kid": "kid",' +
        '"audience": "aud",' +
        '"expiry": 5000,' +
        '"privateKey": "---Private Key---"'+
    '}';

    beforeEach(function () {

        mockFs = jasmine.createSpyObj('fs', ['exists', 'readFile']);
        asapConfig = requireWithMocks(getRequirePath('lib/asap-config'), {
            'mz/fs': mockFs
        });
    });

    describe('getFromOptions', function() {

        it('will read the passed in config file', willResolve(function () {
            mockFs.exists.and.returnValue(q(true));
            mockFs.readFile.and.returnValue(q(asapConfigFileContent));

            return asapConfig.getFromOptions({configFile: 'configFile'}).then(function() {
                expect(mockFs.exists).toHaveBeenCalledWith('configFile');
                expect(mockFs.readFile).toHaveBeenCalledWith('configFile', 'utf8');
                expect(mockFs.exists.calls.count()).toEqual(1);
                expect(mockFs.readFile.calls.count()).toEqual(1);
            });
        }));

        it('will read the default config file (.asap-config) when no configFile is passed in', willResolve(function () {
            mockFs.exists.and.callFake(function(filename) {
                if (filename === './.asap-config') {
                    return q(true);
                }
                return q(false);
            });
            mockFs.readFile.and.returnValue(q(asapConfigFileContent));

            return asapConfig.getFromOptions({}).then(function() {
                expect(mockFs.exists).toHaveBeenCalledWith('./.asap-config');
                expect(mockFs.readFile).toHaveBeenCalledWith('./.asap-config', 'utf8');
                expect(mockFs.exists.calls.count()).toEqual(1);
                expect(mockFs.readFile.calls.count()).toEqual(1);
            });
        }));

        it('will read the non-hidden config file (asap-config) when .asap-config is not present', 
            willResolve(function () {
                
                mockFs.exists.and.callFake(function(filename) {
                    if (filename === './asap-config') {
                        return q(true);
                    }
                    return q(false);
                });
                mockFs.readFile.and.returnValue(q(asapConfigFileContent));
                
                return asapConfig.getFromOptions({}).then(function() {
                    expect(mockFs.exists).toHaveBeenCalledWith('./asap-config');
                    expect(mockFs.readFile).toHaveBeenCalledWith('./asap-config', 'utf8');
                    expect(mockFs.exists.calls.count()).toEqual(2);
                    expect(mockFs.readFile.calls.count()).toEqual(1);
                });
            }));

        it('will read config file if present', willResolve(function () {
            mockFs.exists.and.returnValue(q(true));
            return asapConfig.getFromOptions({configFile: 'configFile'}).then(function() {
                expect(mockFs.readFile).toHaveBeenCalledWith('configFile', 'utf8');
            });
        }));

        it('will parse the config file correctly if present', willResolve(function () {
            mockFs.exists.and.returnValue(q(true));
            mockFs.readFile.and.returnValue(q(asapConfigFileContent));
            return asapConfig.getFromOptions({}).then(function(options) {
                expect(options.issuer).toEqual('issuer');
                expect(options.sub).toEqual('subject');
                expect(options.audience).toEqual('aud');
                expect(options.kid).toEqual('kid');
                expect(options.expiry).toEqual(5000);
                expect(options.privateKey).toEqual('---Private Key---');
            });
        }));

        it('will override the config file with the passed in options', willResolve(function () {
            mockFs.exists.and.returnValue(q(true));
            mockFs.readFile.and.returnValue(q(asapConfigFileContent));
            
            var newExpiry = '90';
            var newAudience = 'Audience';
            var newIssuer = 'New Issuer';
            var newSub = 'New Subject';
            var newPrivateKey = 'privateKey';
            var newKid = 'newKid';
            
            return asapConfig.getFromOptions({
                expiry: newExpiry,
                audience: newAudience,
                issuer: newIssuer,
                sub: newSub,
                privateKey: newPrivateKey,
                kid: newKid
            }).then(function(options) {
                expect(options.issuer).toEqual(newIssuer);
                expect(options.sub).toEqual(newSub);
                expect(options.audience).toEqual(newAudience);
                expect(options.kid).toEqual(newKid);
                expect(options.expiry).toEqual(parseInt(newExpiry));
                expect(options.privateKey).toEqual(newPrivateKey);
            });
        }));

        it('will read the private key from the privateKeyFile if it is passed in options', willResolve(function() {

            var privateKeyFile = 'pk.pem';
            var privateKeyFileContent = '--Private Key From File--';

            mockFs.exists.and.returnValue(q(true));
            mockFs.readFile.and.callFake(function(path) {
                if (path === './.asap-config') {
                    return q(asapConfigFileContent);
                } else if (path === privateKeyFile) {
                    return q(privateKeyFileContent);
                }
                return q();
            });

            return asapConfig.getFromOptions({privateKeyFile: privateKeyFile}).then(function(options) {
                expect(options.issuer).toEqual('issuer');
                expect(options.sub).toEqual('subject');
                expect(options.audience).toEqual('aud');
                expect(options.kid).toEqual('kid');
                expect(options.expiry).toEqual(5000);
                expect(options.privateKey).toEqual(privateKeyFileContent);
            });
        }));

        it('will prefer the private key from the privateKeyFile if both privateKey and file is passed in options',
            willResolve(function() {
                var privateKeyFile = 'pk.pem';
                var privateKeyFileContent = '--Private Key--';

                mockFs.exists.and.returnValue(q(true));
                mockFs.readFile.and.callFake(function(path) {
                    if (path === './.asap-config') {
                        return q(asapConfigFileContent);
                    } else if (path === privateKeyFile) {
                        return q(privateKeyFileContent);
                    }
                    return q();
                });

                return asapConfig.getFromOptions({privateKeyFile: privateKeyFile, privateKey: 'prvKey'})
                    .then(function(options) {
                        expect(options.issuer).toEqual('issuer');
                        expect(options.sub).toEqual('subject');
                        expect(options.audience).toEqual('aud');
                        expect(options.kid).toEqual('kid');
                        expect(options.expiry).toEqual(5000);
                        expect(options.privateKey).toEqual(privateKeyFileContent);
                    });
            }));

        it('will read the private key from config if the passed in privateKeyFile is not present',
            willResolve(function() {

                var privateKeyFile = 'pk.pem';
                var privateKeyFileContent = '--Private Key--';
    
                mockFs.exists.and.callFake(function(path) {
                    if (path === './.asap-config') {
                        return q(true);
                    } else if (path === privateKeyFile) {
                        return q(false);
                    }
                    return q();
                });
                mockFs.readFile.and.callFake(function(path) {
                    if (path === './.asap-config') {
                        return q(asapConfigFileContent);
                    } else if (path === privateKeyFile) {
                        return q(privateKeyFileContent);
                    }
                    return q();
                });
    
                return asapConfig.getFromOptions({privateKeyFile: privateKeyFile}).then(function(options) {
                    expect(options.issuer).toEqual('issuer');
                    expect(options.sub).toEqual('subject');
                    expect(options.audience).toEqual('aud');
                    expect(options.kid).toEqual('kid');
                    expect(options.expiry).toEqual(5000);
                    expect(options.privateKey).not.toEqual(privateKeyFileContent);
                });
            }));
    });

    describe('validate', function () {

        it('passes when all the required properties are present', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                audience: 'aud',
                kid: 'kid',
                privateKey: 'privateKey'
            };

            return asapConfig.validate(config);
        }));

        it('passes when audience is an array', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                audience: ['aud1', 'aud2'],
                kid: 'kid',
                privateKey: 'privateKey'
            };

            return asapConfig.validate(config);
        }));

        it('passes when sub is present', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                sub: 'Subject',
                audience: 'aud',
                kid: 'kid',
                privateKey: 'privateKey'
            };

            return asapConfig.validate(config);
        }));

        it('rejects promise when kid is missing', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                audience: 'aud',
                privateKey: 'privateKey'
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));

        it('rejects promise when kid is empty string', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                audience: 'aud',
                kid: '',
                privateKey: 'privateKey'
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));

        it('rejects promise when aud is missing', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                kid: 'kid',
                privateKey: 'privateKey'
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));

        it('rejects promise when audience is an empty array', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                kid: 'kid',
                privateKey: 'privateKey',
                audience: []
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));

        it('rejects promise when audience is an array with empty string', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                kid: 'kid',
                privateKey: 'privateKey',
                audience: ['', '']
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));

        it('rejects promise when issuer is missing', willResolve(function () {
            var config = {
                kid: 'kid',
                audience: 'aud',
                privateKey: 'privateKey'
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));

        it('rejects promise when privateKey is missing', willResolve(function () {
            var config = {
                issuer: 'Issuer',
                audience: 'aud',
                kid: 'kid'
            };

            return expectToReject(asapConfig.validate(config))
                .then(function (err) {
                    expect(err).toBeTruthy();
                });
        }));
    });
});
