var requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();
var expandTilde = require('expand-tilde');

describe('unit/commands', function () {
    
    var jwtUtilsSpy;
    var asapConfigSpy;
    var proxySpy;
    
    
    var commands;
    
    beforeEach(function () {

        jwtUtilsSpy = jasmine.createSpyObj('jwtUtils', ['getJwtAuthHeader']);
        asapConfigSpy = jasmine.createSpyObj('asapConfig', ['getFromOptions', 'validate']);
        proxySpy = jasmine.createSpyObj('proxy', ['onError', 'onRequest', 'listen']);
        
        commands = requireWithMocks('../../lib/commands', {
            './asap-config': asapConfigSpy,
            './jwt-utils': jwtUtilsSpy,
            'http-mitm-proxy': function() {
                return proxySpy;
            }
        });

        spyOn(console, 'log');
    });
    
    describe('token', function () {

        var programOptions;

        beforeEach(function () {
            programOptions = {};
        });

        it('should call asapConfig.getFromOptions with the right params', willResolve(function () {
            var command = commands.token(programOptions);
            
            return command()
                .then(function () {
                    expect(asapConfigSpy.getFromOptions).toHaveBeenCalledWith(programOptions);
                });
        }));

        it('should call asapConfig.validate with the right params', willResolve(function () {
            var options = {};
            asapConfigSpy.getFromOptions.and.returnValue(options);

            var command = commands.token(programOptions);

            return command()
                .then(function () {
                    expect(asapConfigSpy.validate).toHaveBeenCalledWith(options);
                });
        }));

        it('should call jwtUtils with the right params', willResolve(function () {
            var options = {};
            asapConfigSpy.validate.and.returnValue(options);

            var command = commands.token(programOptions);

            return command()
                .then(function () {
                    expect(jwtUtilsSpy.getJwtAuthHeader).toHaveBeenCalledWith(options);
                });
        }));

        it('should log the header in console', willResolve(function () {
            jwtUtilsSpy.getJwtAuthHeader.and.returnValue('AuthHeader');
            var command = commands.token(programOptions);

            return command()
                .then(function () {
                    expect(console.log).toHaveBeenCalledWith('AuthHeader');
                });
        }));
    });

    describe('proxy', function () {

        var programOptions;

        beforeEach(function () {
            programOptions = {};
        });

        it('should call asapConfig.getFromOptions with the right params', willResolve(function () {
            var command = commands.proxy(programOptions);

            return command({})
                .then(function () {
                    expect(asapConfigSpy.getFromOptions).toHaveBeenCalledWith(programOptions);
                });
        }));

        it('should call asapConfig.validate with the right params', willResolve(function () {
            var options = {};
            asapConfigSpy.getFromOptions.and.returnValue(options);

            var command = commands.proxy(programOptions);

            return command({})
                .then(function () {
                    expect(asapConfigSpy.validate).toHaveBeenCalledWith(options);
                });
        }));

        var expectListen = function(port) {
            expect(proxySpy.listen).toHaveBeenCalledWith(jasmine.objectContaining({
                port: { port: port, host: 'localhost' },
                sslCaDir: expandTilde('~/.http-mitm-proxy'),
                silent: true
            }), jasmine.any(Function));
        };

        it('should call listen with default port', willResolve(function () {
            var command = commands.proxy(programOptions);

            return command({})
                .then(function () {
                    expectListen(8888);
                });
        }));

        it('should call listen with custom port', willResolve(function () {
            var command = commands.proxy(programOptions);

            return command({port: 8889})
                .then(function () {
                    expectListen(8889);
                });
        }));
    });

});
