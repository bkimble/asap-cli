var path = require('path');

global.getRequirePath = function getRequirePath(pathRelativeToRoot) {
    return path.normalize(__dirname + '/../../' + pathRelativeToRoot);
};
