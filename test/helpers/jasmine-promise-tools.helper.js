'use strict';

const jasminePromiseTools = require('jasmine-promise-tools');

global.willResolve = jasminePromiseTools.willResolve;
global.expectToReject = jasminePromiseTools.expectToReject;
